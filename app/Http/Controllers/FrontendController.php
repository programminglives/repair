<?php

namespace App\Http\Controllers;

use App\Mail\ContactUsMessageReceived;
use App\Models\Message;
use App\Models\Service;
use App\Models\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FrontendController extends Controller
{
    public function index() {
        $services = Service::all();
        $testimonials = Testimonial::all();
        return view('index', compact('services', 'testimonials'));
    }

    public function about() {
        return view('about');
    }

    public function contact() {
        return view('contact');
    }

    public function saveContact(Request $request) {

        $validated = $request->validate([
            'name' => 'required|string',
            'phone_number' => 'required',
            'email' => 'required|email',
            'message' => 'required',
        ]);

        $message = Message::create($validated);
        Mail::to($request->email)->send(new ContactUsMessageReceived($message));
        return redirect()->back()->with('success', 'Your message has been received');
    }
}

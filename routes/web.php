<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\AdminAuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


// ------------------------ Start Frontend Routes -------------------------------- //
Route::get('/', [FrontendController::class, 'index']);
Route::get('/contact', [FrontendController::class, 'contact']);
Route::get('/about', [FrontendController::class, 'about']);
Route::post('/save-contact', [FrontendController::class, 'saveContact']);
// ------------------------ End Frontend Routes -------------------------------- //

// ------------------------ Start Dashboard Routes -------------------------------- //
Route::get('admin/login', [AdminAuthController::class, 'login'])->name('admin.login')->middleware('guest:admin');
Route::post('admin/login', [AdminAuthController::class, 'store']);
Route::group(['middleware' => 'auth:admin', 'prefix' => '/admin/', 'as' => 'admin.'], function(){
    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('messages', [MessageController::class, 'index']);
    Route::get('messages/{id}/delete', [MessageController::class, 'destroy']);
// ----------Start CRUD for Service---------- //
    Route::get('service', [ServiceController::class, 'index'])->name('service.index');
    Route::get('service/create', [ServiceController::class, 'create'])->name('service.create');
    Route::post('service/store', [ServiceController::class, 'store'])->name('service.store');
    Route::get('service/{id}/edit', [ServiceController::class, 'edit'])->name('service.edit');
    Route::post('service/{id}/update', [ServiceController::class, 'update'])->name('service.update');
    Route::get('service/{id}/delete', [ServiceController::class, 'delete'])->name('service.destroy');
// ----------End CRUD for Service---------- //
    Route::post('admin/logout', [AdminAuthController::class, 'destroy'])->name('logout');
});
// ------------------------ End Dashboard Routes -------------------------------- //

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
